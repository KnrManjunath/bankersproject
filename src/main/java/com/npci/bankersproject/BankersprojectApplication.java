package com.npci.bankersproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankersprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankersprojectApplication.class, args);
	}

}
